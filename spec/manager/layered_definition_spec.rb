require 'spec_helper'

class DefinitionTestClass
  include UltraCommandLine::Manager::LayeredDefinition
end

describe UltraCommandLine::Manager::LayeredDefinition do

  let (:extra_definition) do
    {
        yo: :man,
        foo: :bar
    }
  end

  subject { DefinitionTestClass.new }

  it 'should provide the #definition_hash as a simple Hash' do
    expect(subject.definition_hash).to be_a Hash
  end

  it '#definition_hash should be empty by default' do
    expect(subject.definition_hash).to be_empty
  end

  context 'when initialized from a hash' do

    subject do
      s = DefinitionTestClass.new
      s.initialize_definition extra_definition
      s
    end

    it 'definitions in hash should be present in the resulting #definition_hash' do
      expect(subject.definition_hash[:yo]).to eq :man
    end

    it 'original definitions should be kept in the dedicated "base" layer' do
      manager = subject.send :layers_manager
      expect(manager.layers['base']).to eq extra_definition
      expect(manager.layers['manual_updates']).to be_empty
    end

    context 'if ' do

    end


  end

  context 'when calling #contribute_to_definition to provide extra definition' do

    let(:extra_data_set) { { a: :b } }
    subject do
      s = DefinitionTestClass.new
      s.contribute_to_definition extra_definition
      s
    end

    it 'extra definitions should be present in the resulting #definition_hash' do
      expect(subject.definition_hash[:yo]).to eq :man
    end

    it 'extra definitions should be kept in a dedicated layer arbitrary named' do
      manager = subject.send :layers_manager
      expect(manager.layers['base']).to be_empty
      expect(manager.layers['manual_updates']).to be_empty
      expect(manager.layers['unknown_layer']).to eq extra_definition
    end

    it 'should be possible to name the layer' do
      subject.contribute_to_definition extra_data_set, layer_name: :my_layer
      manager = subject.send :layers_manager
      expect(manager.layers['base']).to be_empty
      expect(manager.layers['manual_updates']).to be_empty
      expect(manager.layers['unknown_layer']).to eq extra_definition
      expect(manager.layers['my_layer']).to eq extra_data_set
      expect(subject.definition_hash[:a]).to eq :b
    end

  end

  context 'when updating the #definition_hash manually' do

    subject do
      s = DefinitionTestClass.new
      s[:foo] = :bar
      s
    end

    it 'changes should be visible in the resulting #definition_hash' do
      expect(subject.definition_hash[:foo]).to eq :bar
    end

    it 'modifications should have been kept in the dedicated "manual_updates" layer' do
      manager = subject.send :layers_manager
      expect(manager.layers['base']).to be_empty
      expect(manager.layers['manual_updates'][:foo]).to eq :bar
    end

    context 'when resetting the #definition_hash' do

      it 'should be back to original state' do
        expect {subject.send :reset_definition_hash}.not_to raise_error
        expect(subject.definition_hash).to be_empty
      end

    end

  end


end