module InvalidProcessor
  class NoMethod

  end
  class CheckParams
    def check_params
    end
  end
  class Execute
    def execute; end
  end
end

class BaseTestProcessor
  def check_params(*args)
    UltraCommandLine.logger.debug 'Checking params test processor'
  end
  def execute
    UltraCommandLine.logger.debug 'Executing test processor'
  end
end

class TestProcessorNotAcceptingParams < BaseTestProcessor
  def check_params(*args)
    super
    false
  end
end

class TestProcessorAcceptingParams < BaseTestProcessor
  def check_params(*args)
    super
    true
  end
end