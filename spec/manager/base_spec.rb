require 'spec_helper'
require File.expand_path File.join('..', 'processors_test_implementations'), __FILE__

describe UltraCommandLine::Manager::Base do

  let(:tests_path) { File.expand_path File.join('..', '..', '..', 'test'), __FILE__  }

  let(:yaml_test_file) { File.join tests_path, 'simple_command.yml' }

  subject { described_class.from_yaml_file yaml_test_file }

  it 'should create hierarchy from yaml' do
    expect { subject }.not_to raise_error
  end

  it 'should transform the yaml into a set of commands' do
    expect(subject.commands.size).to eq 1
    expect(subject.commands.first.options.size).to eq 3
  end


  context 'when building definition incrementally' do

    subject { described_class.new }

    let(:yaml_as_hash) { YAML.load File.read yaml_test_file }

    it 'should be possible to regenerate commands whenever needed' do
      expect { subject.contribute_to_definition yaml_as_hash, layer_name: 'My layer'}.not_to raise_error
      expect(subject.commands).to be_empty
      expect { subject.definition_hash_to_commands }.not_to raise_error
      expect(subject.commands).not_to be_empty
    end
  end


  context 'when evaluating command line' do

    let(:cmd_line_params) { %w(--help) }
    let(:cmd_line_params_with_extra) { %w(unexpected --help) }

    it 'should have no processor by default' do
      expect { subject.processor }.to raise_error UltraCommandLine::Error
    end

    it 'should never pass the sub_command_name' do

    end

    context 'when commands have processors defined' do
      subject do
        s = described_class.from_yaml_file yaml_test_file
        s.commands.each do |command|
          s.register_processor command, processor
        end
        s
      end
      context 'when accepting command line' do
        let(:processor) {TestProcessorAcceptingParams.new}

        it 'should find a suitable processor processor' do
          expect { subject.cmd_line_args = cmd_line_params }.not_to raise_error
          expect { subject.processor }.not_to raise_error
          expect { subject.cmd_line_args = cmd_line_params_with_extra }.not_to raise_error
          expect { subject.processor }.not_to raise_error
          expect { subject.processor.execute }.not_to raise_error
        end

      end

      context 'when not accepting command line' do
        let(:processor) {TestProcessorNotAcceptingParams.new}

        it 'should NOT find a suitable processor processor' do
          expect { subject.cmd_line_args = cmd_line_params }.not_to raise_error
          expect { subject.processor }.to raise_error
          expect { subject.cmd_line_args = cmd_line_params_with_extra }.not_to raise_error
          expect { subject.processor }.to raise_error
        end
      end

    end

  end


  context 'when the Yaml does not define any option' do

    let(:yaml_test_file) { File.join tests_path, 'simple_command_with_no_option.yml' }

    it 'should still work' do
      expect { subject }.not_to raise_error
    end

  end


  context 'when the yaml defines subcommands' do

    let(:yaml_test_file) { File.join tests_path, 'multiple_commands.yml' }

    let(:cmd_line_params) { %w(sub2 --help) }
    let(:processor) { TestProcessorAcceptingParams.new }

    subject do
      s = described_class.from_yaml_file yaml_test_file
      command = s.command_by_alias :sub2
      s.register_processor command, processor
      s
    end

    it 'should create hierarchy from yaml' do
      expect { subject }.not_to raise_error
      expect(subject.commands.size).to eq 3

      subject.commands.each do |command|
        expect(command.options.size).to eq 2
      end
    end

    it 'should route the request to the right command' do
      expect { subject.cmd_line_args = cmd_line_params }.not_to raise_error
      expect { subject.processor }.not_to raise_error
      expect(subject.processor).to eq processor
    end

    context 'when options are global' do

      let(:yaml_test_file) { File.join tests_path, 'multiple_commands_with_common_options.yml' }

      it 'the option should answer to #global?' do
        expect(subject.root_command.options.select(&:global?).map(&:name)).to include 'help'
      end

      it 'should be visible by all sub commands' do
        subject.commands.each do |command|
          # puts "command #{command.name} contains options #{command.options.map(&:name).inspect}"
          expect(command.options.map(&:name)).to include 'help'
        end
      end
    end


    context 'when multiple processors can reply to the same command line parameters' do
      let(:option1) { UltraCommandLine::Commands::OptionDefinition.new :help, :bool }
      let(:option2) { UltraCommandLine::Commands::OptionDefinition.new :option2, :bool }
      let(:option3) { UltraCommandLine::Commands::OptionDefinition.new :option3, :bool }
      let(:option4) { UltraCommandLine::Commands::OptionDefinition.new :option4, :string }

      let(:yaml_test_file) { File.join tests_path, 'multiple_commands.yml' }
      # let(:extra_command) {
      #   UltraCommandLine::Commands::SubCommand.new :sub2, [option1, option2, option3, option4]
      # }

      context 'when it cannot determine from the whole command line' do

        # 'help' is an option declared both in the yam file for :sub2 and in the extra command :sub2 defined here above
        # Then --help cannot be determined alone
        let(:cmd_line_params) { %w(sub2 --help) }
        subject do
          s = described_class.from_yaml_file yaml_test_file
          # extra_command = UltraCommandLine::Commands::SubCommand.new s,:sub2, options: [option1, option2, option3, option4]
          # s.commands << extra_command
          s.commands.each do |command|
            s.register_processor command, processor
            s.register_processor command, processor
          end
          s
        end

        it 'should issue an exception' do
          # expect { subject.commands << extra_command }.not_to raise_error
          expect { subject.cmd_line_args = cmd_line_params }.not_to raise_error
          expect { subject.processor }.to raise_error UltraCommandLine::Error
        end

      end

      context 'when can determine from the whole command line' do

        let(:cmd_line_params_examples) do
          [
              %w(sub2 --verbose),
              %w(sub2 --option2),
              %w(sub2 --help --option2),
              %w(sub2 --help --verbose)
          ]
        end

        it 'should work as expected' do
          # expect { subject.commands << extra_command }.not_to raise_error
          cmd_line_params_examples.each do |cmd_line_params|
            expect { subject.cmd_line_args = cmd_line_params }.not_to raise_error
            expect { subject.processor }.not_to raise_error
          end
        end

      end

    end


  end


end