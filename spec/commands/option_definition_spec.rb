require 'spec_helper'

describe UltraCommandLine::Commands::OptionDefinition do
  let(:option_name) { :test_option }
  let(:option_type) { :bool }

  subject { described_class.new option_name, option_type }

  it 'should contain all necessary data for a slop option' do
    expect(subject.name).to eq option_name
    expect(subject.type).to eq option_type
    expect(subject.long_aliases).to be_a Array
    expect(subject.long_aliases).to be_empty
    expect(subject.short_aliases).to be_a Array
    expect(subject.short_aliases).to be_empty
    expect(subject.summary).to eq described_class::DEFAULT_SUMMARY
    expect(subject.description).to eq described_class::DEFAULT_SUMMARY
    expect(subject.help).to eq described_class::DEFAULT_SUMMARY
  end

  it 'should be able to generate a slop option' do
    opts = nil
    expect { opts = subject.to_slop_options }.not_to raise_error
    expect(opts).to be_a Slop::Options
    expect(opts.options.first).to be_a Slop::BoolOption
  end

  context 'when option type is not valid' do

    let(:option_type) { :non_existing_type }

    it 'should raise an exception' do
      expect { subject.to_slop_options }.to raise_error UltraCommandLine::Error
    end

  end

  context 'when option type is a string' do

    let(:option_type) { :string }

    it 'should not raise an exception' do
      expect { subject.to_slop_options }.not_to raise_error
    end

  end


  context 'when specifying all options' do

    subject { described_class.new option_name, option_type, short_options: :h, long_options: :help, summary: 'Displays this help.', default: true }

    it 'should work as well' do
      expect { subject.to_slop_options }.not_to raise_error
    end

  end

end