require 'spec_helper'

describe UltraCommandLine::Commands::SubCommand do
  let(:manager) { UltraCommandLine::Manager::Base.new }
  let(:valid_name) { 'sub1' }

  subject { described_class.new manager, valid_name }

  it 'should have a name to scope commands' do
    expect(subject.name).to eq valid_name
  end

  it 'should have a default type' do
    expect(subject.type).to eq :bool
  end

  context 'when name is empty' do

    let(:main_command_module) { UltraCommandLine::Commands::MainCommand }
    subject { described_class.new manager }

    it 'should be decorated as MainCommand' do
      expect(subject.singleton_class.ancestors).to include(main_command_module)
    end

  end

  context 'when commands are specified' do

    let(:option_definition1) { UltraCommandLine::Commands::OptionDefinition.new :option_definition1, :bool }
    let(:option_definition2) { UltraCommandLine::Commands::OptionDefinition.new :option_definition2, :bool }
    let(:option_definition3) { UltraCommandLine::Commands::OptionDefinition.new :option_definition3, :bool }
    let(:option_definition4) { UltraCommandLine::Commands::OptionDefinition.new :option_definition4, :string }
    let(:cmd_line_args) { %w(test_subcommand --option_definition1 --option_definition2 --option_definition4 foo ) }

    let(:subcommand_name) { :test_subcommand }

    subject do
      s = described_class.new manager, subcommand_name
      s.options << option_definition1 << option_definition2 << option_definition3 << option_definition4
      s.banner = 'This is a banner text'
      s
    end

    it 'should issue a command line parser' do
      manager.cmd_line_args = cmd_line_args
      expect(subject.cmd_line_args).to be_included_in cmd_line_args
      expect(subject.cmd_line_args.size).to be cmd_line_args.size - 1
      expect(subject.options.size).to eq 4
    end

    it 'should be able to return an option hash from command line parameters' do
      manager.cmd_line_args = cmd_line_args
      expect(subject.params_hash[:option_definition1]).to be_truthy
      expect(subject.params_hash[:option_definition2]).to be_truthy
      expect(subject.params_hash[:option_definition3]).to be_falsey
      expect(subject.params_hash[:option_definition4]).to eq 'foo'
    end

    context 'when an incorrect option is passed' do

      let(:cmd_line_args) { %w(test_subcommand --option-bad) }

      context 'by default' do


        it 'should raise an exception' do
          manager.cmd_line_args = cmd_line_args
          expect { subject.params_hash }.to raise_error Slop::UnknownOption
        end

      end

      context 'if permissive flag is specified' do

        before(:all) { UltraCommandLine.permissive_mode = true }
        after(:all) { UltraCommandLine.permissive_mode = false }

        it 'should ignore the option' do
          expect(UltraCommandLine).to be_permissive_mode
          manager.cmd_line_args = cmd_line_args
          expect { subject.params_hash }.not_to raise_error Slop::UnknownOption
        end


        # it 'should ignore the option' do
        #   options = UltraCommandLine::DEFAULT_SLOP_OPTIONS.dup
        #   options[:suppress_errors] = true
        #   expect do
        #     subject.to_slop_options options
        #   end.not_to raise_error UltraCommandLine::Error
        # end

      end

    end

    it 'should be able to display a help text' do
      expect { subject.help }.not_to raise_error
    end



    context 'when the command line contains extra parameters' do

      let(:provided_extra_params) { %w(foo bar) }
      let(:subcommand_name) { %w(test_subcommand)}
      let(:cmd_line_args) { subcommand_name + %w(--option_definition4 foo ) + provided_extra_params }

      it 'should be possible to get those parameters' do
        manager.cmd_line_args = cmd_line_args
        expect(subject.extra_arguments).to eq provided_extra_params
      end

    end

    context 'when the option is a multi-word option' do

      context 'when it uses dashes as separator' do

        let(:option_definition1) { UltraCommandLine::Commands::OptionDefinition.new :'multi-words', :bool }

        let(:cmd_line_args) { %w(--multi-words) }

        it 'should support it' do
          manager.cmd_line_args = cmd_line_args
          expect(subject.params_hash.keys).to include :'multi-words'
        end

      end

      context 'when it uses underscores as separator' do

        let(:option_definition1) { UltraCommandLine::Commands::OptionDefinition.new :'multi_words', :bool }

        let(:cmd_line_args) { %w(--multi_words) }

        it 'should support it' do
          manager.cmd_line_args = cmd_line_args
          expect(subject.params_hash.keys).to include :'multi_words'
        end

      end


    end


    context 'when some parameters have dependencies' do

      let(:tests_path) { File.expand_path File.join('..', '..', '..', 'test'), __FILE__  }

      let(:yaml_test_file) { File.join tests_path, 'simple_command_hierarchy.yml' }
      let(:cmd_line_params_invalid_examples) do
        [
            %w(--extra),
            %w(--extra --help),
            %w(--extra --verbose),
            %w(--extra --verbose --help),
            %w(--awesome),
            %w(--awesome --extra --version)
        ]
      end
      let(:cmd_line_params_valid_examples) do
        [
            %w(--extra --version),
            %w(--extra --version --help),
            %w(--extra --version --verbose),
            %w(--extra --version --verbose --help),
            %w(--awesome --extra --version --verbose)
        ]
      end

      subject do
        s = described_class.from_yaml_file yaml_test_file
        s.send :manager=, manager
        manager.commands << s
        s
      end

      it 'should raise error when dependencies are not met' do
        cmd_line_params_invalid_examples.each do |cmd_line_params|
          # STDERR.puts "cmd line params:  #{cmd_line_params}"
          manager.cmd_line_args = cmd_line_params
          expect(subject).not_to be_valid
        end
      end

      it 'should be valid when dependencies are met' do
        cmd_line_params_valid_examples.each do |cmd_line_params|
          # STDERR.puts "cmd line params:  #{cmd_line_params}"
          manager.cmd_line_args = cmd_line_params
          expect(subject).to be_valid
        end
      end

    end


  end

end
