# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ultra_command_line/version'

Gem::Specification.new do |spec|
  spec.name          = 'ultra_command_line'
  spec.version       = UltraCommandLine::VERSION
  spec.authors       = ['Laurent B.']
  spec.email         = ['lbnetid+rb@gmail.com']

  spec.summary       = %q{Manage complex sub_command line options.}
  spec.description   = %q{Allows to handle complex sub_command line options with subcommands a-la-git.}
  spec.homepage      = 'https://gitlab.com/lbriais/ultra_command_line'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'simplecov'

  spec.add_dependency 'super_stack', '~> 1.0'
  spec.add_dependency 'slop', '~> 4.5'
end
