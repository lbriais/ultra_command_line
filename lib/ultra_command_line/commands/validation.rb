module UltraCommandLine
  module Commands

    module Validation


      def valid?(raise_error: false)
        # return false_or_raise 'You have to successfully parse cmd line parameters before checking its validity !', raise_error: raise_error  if params_hash.nil?
        begin
          raise if params_hash.nil?
        rescue
          return false_or_raise 'You have to successfully parse cmd line parameters before checking its validity !', raise_error: raise_error
        end
        return false unless check_options_dependencies raise_error: raise_error
        return false unless check_options_incompatibilities raise_error: raise_error
        if name.empty?
          unless root_command?
            return false_or_raise 'Only a root command should not have a name !', raise_error: raise_error
          end
        end
        true
      end

      private

      def check_options_incompatibilities(raise_error: false)
        options.each do |option|
          if option_eligible_for_incompatibility_test? option
            option.incompatibilities.each do |incompatibility|
              unless params_hash[incompatibility.to_sym].nil? or params_hash[incompatibility.to_sym] == false
                return false_or_raise "Command line option '#{option.name}' is incompatible with option '#{incompatibility}' !",
                                      raise_error: raise_error,
                                      error_type: UltraCommandLine::OptionDependencyError
              end
            end
          end
        end
        true
      end

      def option_eligible_for_incompatibility_test?(option)
        return false if option.incompatibilities.empty?
        keys_str = params_hash.keys.map &:to_s
        if keys_str.include? option.name
          not (params_hash[option.name.to_sym].nil? or params_hash[option.name.to_sym] == false)
        else
          false
        end
      end


      def check_options_dependencies(raise_error: false)
        options.each do |option|
          if option_eligible_for_dependency_test? option
            option.dependencies.each do |dependency|
              if params_hash[dependency.to_sym].nil? or params_hash[dependency.to_sym] == false
                return false_or_raise "Command line option '#{option.name}' requires option '#{dependency}' !",
                                      raise_error: raise_error,
                                      error_type: UltraCommandLine::OptionDependencyError
              end
            end
          end
        end
        true
      end

      def option_eligible_for_dependency_test?(option)
        return false if option.dependencies.empty?
        keys_str = params_hash.keys.map &:to_s
        if keys_str.include? option.name
          not (params_hash[option.name.to_sym].nil? or params_hash[option.name.to_sym] == false)
        else
          false
        end
      end

    end

  end
end