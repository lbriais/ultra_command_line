module UltraCommandLine
  module Commands

    module HelpFormatter

      DEFAULT_SEPARATOR_WIDTH = 80
      DEFAULT_SEPARATOR_FILLER = '-'.freeze
      DEFAULT_TITLE = 'Options'.freeze

      attr_writer :separator_width, :separator_filler

      def separator_width
        @separator_width ||= DEFAULT_SEPARATOR_WIDTH
      end

      def title
        name.nil? || name.empty? ? DEFAULT_TITLE : name
      end

      def separator_filler
        @separator_filler ||= DEFAULT_SEPARATOR_FILLER
      end

      def help
        output = []
        output << banner
        unless options.empty?
          output << build_separator(title)
          output.concat options.sort{|a,b| a.name <=> b.name }.map(&:help_line)
        end
        output
      end

      private

      def build_separator(title)
        "#{separator_filler * 2} #{title} ".ljust separator_width, separator_filler
      end

    end

  end
end
