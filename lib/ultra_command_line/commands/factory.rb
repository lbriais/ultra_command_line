module UltraCommandLine
  module Commands

    module Factory

      DEFAULT_BANNER = ''.freeze
      DEFAULT_COMMAND_TYPE = :bool

      include UltraCommandLine::Utils::YamlFactory

      def from_hash(definition_hash, factory_options = {})
        name = factory_options.fetch :name, ''
        options_definition_hash = definition_hash.fetch(:options, {})
        manager = factory_options[:manager]
        banner = definition_hash.fetch :banner, DEFAULT_BANNER
        type = definition_hash.fetch(:type, DEFAULT_COMMAND_TYPE).to_sym
        create_command name, type, options_definition_hash, banner, manager
      end

      private

      def create_command(name, type, options_definition_hash, banner, manager)
        options = options_definition_hash.map do |option_name, option_definition_hash|
          option_type = option_definition_hash[:type].to_sym
          UltraCommandLine::Commands::OptionDefinition.new option_name, option_type, option_definition_hash
        end
        new manager, name, type, banner, options: options
      end

    end

  end
end