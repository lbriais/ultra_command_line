require 'ultra_command_line/commands/validation'
require 'ultra_command_line/commands/command_line_parser'
require 'ultra_command_line/commands/main_command'
require 'ultra_command_line/commands/option_definition'
require 'ultra_command_line/commands/help_formatter'
require 'ultra_command_line/commands/factory'

module UltraCommandLine
  module Commands

    class SubCommand

      include UltraCommandLine::Utils::ErrorPropagation

      extend UltraCommandLine::Commands::Factory

      include UltraCommandLine::Commands::Validation
      include UltraCommandLine::Commands::HelpFormatter
      include UltraCommandLine::Commands::CommandLineParser


      attr_reader :name, :type, :manager, :options, :aliases
      attr_accessor :banner

      def initialize(manager,
                     name = '',
                     type = UltraCommandLine::Commands::Factory::DEFAULT_COMMAND_TYPE,
                     banner = UltraCommandLine::Commands::Factory::DEFAULT_BANNER,
                     options: [])
        @manager = manager
        @name = name
        if name.empty?
          class << self; include UltraCommandLine::Commands::MainCommand; end
        end
        self.banner = banner
        register_options(options)
        @aliases = [self.name]
        @type = type
      end

      def root_command?
        singleton_class.ancestors.include? UltraCommandLine::Commands::MainCommand
      end

      def cmd_line_args
        self_view_of_manager_cmd_line_args
      end

      private

      attr_writer :manager

      def register_options(options)
        options.each { |option| option.sub_command = self }
        @options = options
      end

      def self_view_of_manager_cmd_line_args
        manager.cmd_line_args_for_command self
      end

    end

  end
end