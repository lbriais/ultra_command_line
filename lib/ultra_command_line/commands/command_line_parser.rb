module UltraCommandLine
  module Commands

    module CommandLineParser

      def params_hash
        parse_cmd_line_options.to_hash
      end

      def extra_arguments
        parse_cmd_line_options.to_hash
        @extra_arguments ||= []
      end

      private

      def parse_cmd_line_options(options_definition = UltraCommandLine.new_slop_options)
        parser = build_parser(options_definition)
        hash = parser.parse cmd_line_args
        # @cmd_line_args = cmd_line_args
        @extra_arguments = parser.arguments
        hash
      end

      def build_parser(options_definition)
        options_definition.banner = banner
        options.each { |option| option.to_slop_options options_definition }
        Slop::Parser.new options_definition, **options_definition.config
      end

    end

  end
end
