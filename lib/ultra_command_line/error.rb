module UltraCommandLine

  class Error < StandardError; end

  class OptionDependencyError < Error; end

end
