module UltraCommandLine
  module Utils

    module BasicLogger

      class NullLogger
        def method_missing(*args)
          # Do nothing
        end
      end

      def logger=(logger)
        @logger = logger
      end

      def logger
        @logger ||= NullLogger.new
      end

    end

  end
end
