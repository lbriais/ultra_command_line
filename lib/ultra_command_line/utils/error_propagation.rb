module UltraCommandLine
  module Utils

    module ErrorPropagation

      DEFAULT_ERROR_MESSAGE = 'Error message not provided!'.freeze

      private

      def false_or_raise(message = DEFAULT_ERROR_MESSAGE, raise_error: false, error_type: UltraCommandLine::Error)
        if raise_error
          raise error_type, message
        else
          UltraCommandLine.logger.warn message
        end
        false
      end

    end

  end
end
