module UltraCommandLine
  module Utils

    module YamlFactory

      def from_yaml_file(yaml_file, factory_options = {}, &block)
        yaml_file = File.expand_path yaml_file
        UltraCommandLine.logger.debug "Loading commands definitions from '#{yaml_file}'."
        raise UltraCommandLine::Error, 'Invalid Yaml command file specified !' unless File.exists? yaml_file
        raise UltraCommandLine::Error, 'Cannot read Yaml command file !' unless File.readable? yaml_file
        from_yaml File.read(yaml_file), factory_options, &block
      end

      def from_yaml(yaml, factory_options = {}, &block)
        from_hash YAML.load(yaml), factory_options, &block
      rescue => e
        UltraCommandLine.logger.error "#{e.message}\nBacktrace:\n#{e.backtrace.join("\n\t")}"
        raise UltraCommandLine::Error, 'Invalid Yaml content. Parser error !'
      end

    end

  end
end
