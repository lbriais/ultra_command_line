require 'super_stack'

module UltraCommandLine
  module Manager

    module LayeredDefinition

      def definition_hash
        @cached_definition ||= layers_manager[].to_hash
      end

      def initialize_definition(hash)
        reset_cached_configuration
        @layers_manager = setup_layers_manager hash
        self
      end

      def contribute_to_definition(hash, layer_name: :unknown_layer)
        reset_cached_configuration
        layers_manager
        new_layer = SuperStack::LayerWrapper.from_hash hash
        new_layer.name = layer_name
        new_layer.priority = definition_counter
        layers_manager.add_layer new_layer
      end

      def []=(key, value)
        layers_manager[key] = value
      end

      def refresh
        reset_cached_configuration
      end

      def clear
        reset_definition_hash
      end

      private

      BASE_LAYER_PRIORITY = 10
      DEFINITION_LAYERS_BASE_PRIORITY = 100
      UPDATE_LAYER_PRIORITY = 1000

      def reset_cached_configuration
        @cached_definition = nil
      end

      def reset_definition_hash
        reset_cached_configuration
        @layers_manager = nil
      end

      def definition_counter
        raise UltraCommandLine::Error, 'Maximum number of definition reached !' if @definition_counter+1 >= UPDATE_LAYER_PRIORITY
        @definition_counter += 1
      end

      def layers_manager
        @layers_manager ||= setup_layers_manager
      end

      def setup_layers_manager(hash = nil)
        merger = SuperStack::Manager.new
        merger.merge_policy = SuperStack::MergePolicies::FullMergePolicy
        base_layer = if hash.nil?
                       SuperStack::Layer.new
                     else
                       SuperStack::LayerWrapper.from_hash(hash)
                     end
        base_layer.name = :base
        base_layer.priority = BASE_LAYER_PRIORITY
        merger.add_layer base_layer
        update_layer = SuperStack::Layer.new
        update_layer.name = :manual_updates
        update_layer.priority = UPDATE_LAYER_PRIORITY
        merger.add_layer update_layer
        merger.write_layer = update_layer
        @definition_counter = DEFINITION_LAYERS_BASE_PRIORITY
        merger
      end

    end

  end
end
