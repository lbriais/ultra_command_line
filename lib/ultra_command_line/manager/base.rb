require 'ultra_command_line/manager/factory'
require 'ultra_command_line/manager/layered_definition'
require 'ultra_command_line/manager/cmd_line_args'
require 'ultra_command_line/manager/commands'
require 'ultra_command_line/manager/processors'

module UltraCommandLine
  module Manager

    class Base

      include UltraCommandLine::Utils::ErrorPropagation

      extend UltraCommandLine::Manager::Factory

      include UltraCommandLine::Manager::LayeredDefinition
      include UltraCommandLine::Manager::Commands

      include UltraCommandLine::Manager::Processors

      def initialize(commands = [])
        @commands = commands
      end

      def definition_hash_to_commands
        self.class.from_hash(definition_hash) do |commands|
          commands.each {|command| command.send :manager=, self }
          @commands = commands
        end
      end

    end

  end
end
