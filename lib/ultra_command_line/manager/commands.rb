module UltraCommandLine
  module Manager

    module Commands

      include UltraCommandLine::Manager::CmdLineArgs

      def commands
        @commands ||= []
      end

      def root_command
        return nil if commands.empty?
        l = commands.select {|c| c.root_command? && c.name.empty? }
        raise UltraCommandLine::Error, 'Wrong commands definition !' unless l.count == 1
        l.first
      end

      def aliases_consistent?
        # including aliases
        full_names_list = []
        commands.each do |command|
          if command.root_command?
            return false unless command.name.empty? && command.aliases == [command.name]
          else
            command.aliases.each do |alias_name|
              return false if full_names_list.include? alias_name.to_s
              full_names_list << alias_name.to_s
            end

          end
        end
        true
      end

      def command(cmd_line_args = self.cmd_line_args)
        command_alias = cmd_line_args.empty? ? '' : cmd_line_args.first
        command_by_alias(command_alias) || root_command
      end

      def command_by_alias(command_alias)
        raise UltraCommandLine::Error, 'command names/aliases are not consistent !' unless aliases_consistent?
        candidates = commands.select do |command|
          command.aliases.map(&:to_s).include? command_alias.to_s
        end
        # raise UltraCommandLine::Error, "There is no command named '#{command_alias}'!" unless candidates.siez == 1
        candidates.size == 1 ? candidates.first : nil
      end

      def cmd_line_args_for_command(command)
        if command.root_command?
          cmd_line_args
        else
          clfc = cmd_line_args.dup
          clfc.shift
          clfc
        end
      end

      protected

      attr_writer :commands

    end

  end
end
