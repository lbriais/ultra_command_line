module UltraCommandLine
  module Manager

    module Factory

      include UltraCommandLine::Utils::YamlFactory

      def from_hash(definition_hash, factory_options = {})
        UltraCommandLine.logger.debug 'Starting commands analysis from definition hash.'
        commands = []
        # Create main command
        UltraCommandLine.logger.debug 'Defining main command.'
        main_command = create_command '', definition_hash
        main_command.banner = definition_hash.fetch :banner, ''
        commands << main_command
        # Create sub-commands
        definition_hash.fetch(:subcommands, {}).each do |subcommand_name, subcommand_definition|
          UltraCommandLine.logger.debug "Defining sub-command '#{subcommand_name}'."
          # subcommand_definition ||= {}
          subcommand = create_command subcommand_name, subcommand_definition
          subcommand.banner = subcommand_definition.fetch :banner, ''
          # Let's add global options
          main_command.options.select(&:global?).each do |global_option|
            subcommand.options << global_option
          end
          commands << subcommand
        end
        if block_given?
          yield commands
        else
          # Return a new object supporting a list of commands as parameter
          new_manager = new commands
          new_manager.initialize_definition definition_hash
          commands.each {|c| c.send :manager=, new_manager}
          new_manager
        end
      end

      private

      def create_command(name, options_definition)
        UltraCommandLine::Commands::SubCommand.from_hash options_definition, name: name, manager: self
      end



    end

  end
end
