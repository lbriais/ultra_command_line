
module UltraCommandLine
  module Manager

    module CmdLineArgs

      def cmd_line_args
        @cmd_line_args ||= []
      end

      def cmd_line_args=(cmd_line_args)
        cmd_line_args = case cmd_line_args
                          when Array
                            cmd_line_args
                          when String
                            cmd_line_args.split ' '
                        end
        UltraCommandLine.logger.debug "Cmd line: #{cmd_line_args.inspect}"
        @cmd_line_args = cmd_line_args
      end

    end

  end
end
