require 'slop'

require 'ultra_command_line/version'
require 'ultra_command_line/error'
require 'ultra_command_line/utils/basic_logger'
require 'ultra_command_line/utils/error_propagation'
require 'ultra_command_line/utils/yaml_factory'


module UltraCommandLine

  DEFAULT_SLOP_OPTIONS = { underscore_flags: false }

  extend UltraCommandLine::Utils::BasicLogger


  def self.permissive_mode=(permissive_state)
    slop_options[:suppress_errors] = permissive_state
  end

  def self.permissive_mode?
    slop_options[:suppress_errors]
  end

  def self.slop_options
    @slop_options ||= DEFAULT_SLOP_OPTIONS
  end

  def self.new_slop_options
    Slop::Options.new **slop_options
  end

end


require 'ultra_command_line/commands/sub_command'
require 'ultra_command_line/manager/base'

